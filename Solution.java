import java.util.*;

// Write your Checker class here
class Checker implements Comparator<Player>{
    @Override
    public int compare(Player o1, Player o2){ //controller important, dpeesn't compile without
        int s1 = o1.score;
        int s2 = o2.score;
        String name1 = o1.name;
        String name2 = o2.name;
        
        if (s1 != s2){
            if (s1 > s2){
                return -1;
            } else {
                return 1;
            }
        } else {
            if (name1.equals(name2)){
                return 0;
            }
            if (name1.compareTo(name2)>0){
                return 1;
            } else {
                return -1;
            }
        }
    }
    
}

class Player{
    String name;
    int score;
    
    Player(String name, int score){
        this.name = name;
        this.score = score;
    }
}

class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        Player[] player = new Player[n];
        Checker checker = new Checker();
        
        for(int i = 0; i < n; i++){
            player[i] = new Player(scan.next(), scan.nextInt());
        }
        scan.close();

        Arrays.sort(player, checker);
        for(int i = 0; i < player.length; i++){
            System.out.printf("%s %s\n", player[i].name, player[i].score);
        }
    }
}
